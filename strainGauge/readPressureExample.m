%Creates a class variable called arduino that stores the handle for the
%the serial port
arduino = ReadPressure();
arduino.setup('COM4'); %/dev/tty.SLAB_USBtoUART on mac
% '/dev/tty.usbmodem1411' for uno
%pause(5)

%Start Recording
arduino.startAnalog(); % starts the background thread. All data is now recorded
pause(2);
%lastAnalog =  N X 6 array of all the analog readings from the 5 sensors + photo-diode
%lastDigital = N X 7 array of all the digital readings from the 7 sensors (zero or the time-stamp in Arduino time when the key was first pressed)
%flagVal = a code the user sends (see line 24 below) to indicate an event
%lastTimes = N X 1 array of Arduino times in ms since I last called arduino.stratAnalog
%this function immediately returns the last N samples. It can be called any time.
%You should double check the number of items returned because invalid samples will be dropped.
%Thus, the number of returned items could be lower than the number requested.
[lastAnalog, lastDigital, flagVal, lastTimes] = arduino.getLastNSamples(5)

%Baseline at the begnning of the experiment or of each run and substract from all arduino.getLastNSamples(N)
%This allows me to compare the force sensor readings during the experiment to those at baseline when no fingers were on the keys.
Baseline = mean(lastAnalog);


%clearBufer is used to clear the saved values while an experiment is in process. It impacts the future output of getLastNSamples or getAllData. If you are not currently collecting data, then the recommended procedure is to either use 'clear mex', or 'arduino.stopAnalog(), arduino.startAnalog'. The buffer is always cleared when you run startAnalog, but this gives you a way to do it mid experiment in case you have a long running process that could occupy too much memory. In practice, anything under a few hours shouldn't be an issue, but this is available if needed.
arduino.clearBuffer();


%How to collect the identity of the key pressed
%keyout = an array containing the ID (1-7) of up to 7 keys that were pressed
%time = Arduino Time
%timeout = 1 if the time limit was reached before a key was pressed; 0 if a key was pressed before the time limit
%Time = time to wait in seconds (in system time) for a keypress before going on with the program
%If a key is pushed, the program advances before time is up
[keyOut, time, timedOut] = arduino.waitForKeys([1:7], GetSecs + 5); %blocks until a key is pressed, or a timeout is reached. Timeout is given in terms of GetSecs time.

%Limit the rate of data collection from Arduino to X Hz (e.g., 100 Hz)
%This works by inserting a delay between successive samples.
arduino.setTargetHz(100);

%Clear out pressed keys. This will reset the digitValues to zero after a button is pushed.
arduino.resetDigitalState();

%Demo of using arduino.kbcheck and the flag (trial codes)
startTime = GetSecs;
flagVal = 1;
while GetSecs-startTime < 20
    %Checks to see if a key was pressed since the last call to resetDigitalState.
    %If so, it returns the keys and the time when they were pressed.
    [whichKey, pressTime] = arduino.kbcheck(1:7);
    if ~isempty(whichKey)
        arduino.sendFlag(flagVal);%Sends a number up to 32000 (negatives might or might not work)
        flagVal = flagVal + 1;%Increments the flag value (trial code)
        disp(whichKey)
        disp(GetSecs - startTime)
        arduino.resetDigitalState();
    end
end
arduino.stopAnalog(); %stops the background thread.

%Look at times:

%gets all recorded data since calling arduino = ReadPressure().
%This can be done anytime, but is most useful at the end of the experiment to capture all the analog values.
%if I don't want all the data since the beginning of the experiment, I can
%(a) close MATLAB or
%(b) type arduino.close followed by clear mex
[analog, digitalVals, flags, timeVals] = arduino.getAllData;

%find the time index of all of the photo-diode onsets in Column 6 of the analog values
flipIndexes = arduino.findScreenFlips(analog, 900, 1); %inputs are the analog values, a threshold, and a flag for a rising or falling edge. 1 For rising, 0 for falling.
flipTimes = timeVals(flipIndexes);

%find the index (row in the flags array) of all of the flag onsets
flagIndexes = arduino.findFlags(flags);

%Arduino times of flag onsets
flagTimes = timeVals(flagIndexes);

%Gives the analog (force sensor) values at each time when the flag value changes
%Could then check the force senor values before and after these time points
analog(flagIndexes);

%the following block looks at the timeVal data to see the current data rate
diffTime = timeVals(2:end) - timeVals(1:end-1);
mean(diffTime)
1/mean(diffTime)
max(diffTime)
a= find(diffTime>40)

%this block will show a plot for a few seconds

%Closing things out
%clean up by closing the arduino object.
arduino.close()

%this command is used to clear out any captured data. This is the only way to clear the data, othewise another trial will still return this old data when getAllData is called.
clear mex
