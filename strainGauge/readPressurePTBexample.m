% PTB demo of readPressure class: includes photodiode
%this was mostly written to test the photodiode. See ReadPressureExample for all functions

%% setup PTB
Screen('Preference', 'SkipSyncTests', 1);
screens = Screen('Screens');
screenNumber = max(screens);
[windowPtr, windowRect] = PsychImaging('OpenWindow', screenNumber);

[cX, cY] = RectCenter(windowRect);

%Screen('FillRect', windowPtr [,color] [,rect] )

%% setup arduino
arduino = ReadPressure();
arduino.setup('COM4');
arduino.startAnalog();
%% arduino.startAnalog();
pause(1);
[lastAnalog, lastDigital, lastTimes] = arduino.getLastNSamples(5)
arduino.newBaseline(mean(lastAnalog));
%[keyOut, time, timedOut] = arduino.waitForKeys([1:7], GetSecs + 5);
arduino.setTargetHz(100);

%% run something
%test the Arduino->UI->Arduino time
%wait for a keypress, activate the screen change and look for the delay time

for k = 1:10
    [keyOut, time, timedOut] = arduino.waitForKeys([1:7], GetSecs + 5); %blocks until press
    Screen('FillRect', windowPtr, [255, 255, 255], [0, 0, cX, cY] )
    Screen('Flip', windowPtr);
    pause(1)
    Screen('FillRect', windowPtr, [0 0 0], windowRect )
    Screen('Flip', windowPtr);
    pause(2);
end
if 0
    for k = 1:5
        Screen('FillRect', windowPtr, [255, 255, 255], [0, 0, cX, cY] )
        Screen('Flip', windowPtr);
        startTime = GetSecs;
        while GetSecs-startTime < 5
            [whichKey, pressTime] = arduino.kbcheck(1:7);
            if ~isempty(whichKey)
                disp(whichKey)
                disp(GetSecs - startTime)
                arduino.resetDigitalState();
            end
        end
        Screen('FillRect', windowPtr, [0 0 0], windowRect )
        Screen('Flip', windowPtr);
        pause(2);
    end
end


%get all data out of serial device:
[analog, digitalVals, timeVals] = arduino.getAllData;
figure; plot(analog(:,end)); %plot the photodiode output

flipIndexes = arduino.findScreenFlips(analog, 900, 1);
flipTimes = timeVals(flipIndexes);
%% clean up PTB
Screen('CloseAll');

%% clean up arduino
arduino.close()
clear mex
