//Arduino code to work with Daniel Weissman's pressure sensitive buttons

//This requires the Bounce2 library which can be installed from within the Arudino IDE via the "Manage Libraries" menu item.


#include <Bounce2.h>
#include <stdio.h> //sprintf

//TODO: could get rid of bounce since we are holding the digital state until a reset

#define NUM_BUTTONS 7
#define LED 13
const uint8_t BUTTON_PINS[NUM_BUTTONS] = {2, 3, 4, 5, 6, 7, 8};
#define NUM_STRAIN 5
const int StrainGauges[NUM_STRAIN] = {A0, A1, A2, A3, A4};
const int PhotoDiode = A5; //must also connect ground and power
int flag = 0; // flag for current stimulus/response
unsigned long buttonStates[NUM_BUTTONS];

Bounce *buttons = new Bounce[NUM_BUTTONS];

int ledState = LOW;
bool sending = false;
unsigned long startTime = 0;

void setup() {
  //setup bounce pins
  for (int i = 0; i < NUM_BUTTONS; i++) {
    buttons[i].attach( BUTTON_PINS[i] , INPUT_PULLUP  );       //setup the bounce instance for the current button
    buttons[i].interval(25);              // interval in ms
    pinMode(BUTTON_PINS[i], INPUT);
    digitalWrite(BUTTON_PINS[i], HIGH); //internal pullup
    buttonStates[i] = 0;
  }
  pinMode(PhotoDiode, INPUT);

  // Setup the LED :
  pinMode(LED, OUTPUT);
  digitalWrite(LED, ledState);

  //Analog inputs:
//  for(int i = 0; i < NUM_STRAIN; i++){
//
//  }
  Serial.begin(115200); //230400 has read errors, It will seem like it works, but will fail when you try  to send a command longer than 1 character from the comptuer to the Arduino, such as when you set the baud rate, or send a flag.
}


unsigned long lastTime = 0; // hold last micros();
unsigned long microsPerLoop = 0;

void loop() {
  if (Serial.available() > 0) {
    // get incoming byte:
    char inByte = Serial.read();
    if (inByte == 'A'){
      sending = true;
      startTime = millis();
      flag = 0;
    }else if (inByte == 'Z'){
      sending = false;
    }else if (inByte == 'D'){ //adjust data rate
      //use a D command to set the delay for loops
      long desiredHz = Serial.parseInt();
      desiredHz = max(desiredHz, 1);
      desiredHz = min(desiredHz, 10000); //I probably don't need to limit this end. Values over about 450Hz will just fail by being slower than requested
      microsPerLoop = 1000000ul / desiredHz;
//      char outString[50];
//      sprintf(outString, "got desired rate of %ld and calculated %luus\n", desiredHz, microsPerLoop);
//      Serial.print(outString);
    }else if (inByte == 'F'){ //adjust data rate
      flag = Serial.parseInt();
    }else if (inByte == 'C'){//continue sending data. Like A, but no reset
      sending = true;
    }
    if (inByte == 'R' || inByte =='A'){
      //reset digital state on demand, or at new start
      for(int i = 0; i < NUM_BUTTONS; i++){
          buttonStates[i] = 0;
      }
    }

  }

  while(micros() - lastTime < microsPerLoop){
    //wait
  }
  lastTime = micros();//we do this later, so we will always be a little slow, but not enough for anyone to notice
  unsigned long thisTime = millis() - startTime;
  if (sending){
    bool needToToggleLed = false;
    char outString[16]; //max value should be only 12 chars for millis and a space + null
    // put your main code here, to run repeatedly:

    for( int i = 0; i < NUM_STRAIN; i++){
      sprintf(outString, "%04d ", analogRead(StrainGauges[i]));
      Serial.print(outString);
    }

    sprintf(outString, "%04d ", analogRead(PhotoDiode));
    Serial.print(outString);

    for (int i = 0; i < NUM_BUTTONS; i++)  {
      // Update the Bounce instance :
      buttons[i].update();
      // If it fell, note the need to toggle the LED
      if ( buttonStates[i] == 0 && buttons[i].fell() ) {
        needToToggleLed = true;
        //only update if it was previously 0, otherwise keep the older time
        buttonStates[i] = thisTime;
      }
      sprintf(outString, "%lu ", buttonStates[i]);
      Serial.print(outString);
    }

    sprintf(outString, "%d ", flag);
    Serial.print(outString);

    sprintf(outString, "%lu\n", thisTime);
    //sprintf(outString, "%010d\n", 0);
    Serial.print(outString);

    if ( needToToggleLed ) {
      // Toggle LED state :
      ledState = !ledState;
      digitalWrite(LED, ledState);
    }
    //Serial.print("\n"); time print includes newline already
  }// only do processing, serial write if in sending status
}
