classdef Opcodes
    % Normally I'd make this an enumeration, but when passing an
    % enumeration to a mex file, it doesn't evaluate to a regular double
    % value, but instead is always zero.  This is a way around that issue
    % (maybe there's a better way).
    properties (Constant)
        Open                = 1
        Close               = 2
        IsOpen              = 3
        ReadBytes           = 4
        ReadString          = 5
        BytesAvailable      = 6
        FlushIO             = 7
        ClearInputBuffer    = 8
        ReadStrings         = 9
        SetTimeout          = 10
        GetTimeout          = 11
        WriteString         = 12
        WriteBytes          = 13
        BGByteReaderStart   = 14
        BGByteReaderGetData = 15
        BGByteReaderStop    = 16
        
        % These are sketchy commands currently, don't use unless you are a
        % wild and crazy person.
        BGArrayDoubleReaderStart = 17
        BGArrayDoubleReaderGetData = 18
        BGArrayDoubleReaderStop = 19
        BGArrayDoubleReaderIsRunning = 20
        BGArrayDoubleReaderGetLastValue = 21

        BGStringReaderStart = 22
        BGStringReaderGetData = 23
        BGStringReaderStop = 24
        BGStringReaderIsRunning = 25
        BGStringReaderGetLastValue = 26
    end
end
