//test ouptut from the photodiode
//using part nubmer TSL257-LF
//read both digital 10 and analog A5
const int digitalIn = 10;
const int analogIn = A5;
const int LED = 13;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  pinMode(digitalIn, INPUT);
  pinMode(analogIn, INPUT);
  pinMode(LED, OUTPUT);
}

void loop() {

  // put your main code here, to run repeatedly:
  int analogMeasure = analogRead(analogIn);
  int digitalMeasure = digitalRead(digitalIn);
  digitalWrite(LED, digitalMeasure);
  Serial.print(analogMeasure);
  Serial.print(" , ");
  Serial.print(digitalMeasure);
  Serial.print('\n');
}
