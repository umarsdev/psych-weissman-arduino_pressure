classdef ReadPressure < handle
    %
    %pressure sensitive button class based on superserial library

    %arduino = ReadPressure('/dev/cu.usbmodem1451'); %for test device
    %arduino = ReadPressure('/dev/tty.SLAB_USBtoUART'); %for button device (Rugged duino)

    %response values are:
    % 1-5) Analog values for 5 buttons
    % 6) Analog value for photo diode
    % 7-13) 7x digital values. Zero, or the arduino timestamp of the first press
    % 14) User flag. Same flag is sent with every update until it is changed
    % 15) Arduino time since last start command

    properties
        baseline
    end

    methods
        function obj = setup(obj, portName)
            import superserial.Opcodes
            obj.baseline = zeros(1,6);
            disp('# Opening serial port...');
            superserial(Opcodes.Open, portName, 115200);
            pause(2);
            % superserial(Opcodes.ClearInputBuffer);
        end

        function [doubleVals] = stringToDoubleMatrix(~, stringSet)
            %get in a cell array of strings and spit out the double matrix
            nStrings = length(stringSet);
            %assume width of 15 as a simple check that we have valid data
            doubleVals = zeros(nStrings, 15);
            dest = 1;
            for i = 1:nStrings
                newLine = stringSet{i};
                if length(newLine) > 0
                    format = '%f'; %'%ld' isn't working. only get one value??
                    numberVals = sscanf(newLine, format);
                    numberVals = numberVals';
                    if size(numberVals,2) == 15
                        doubleVals(dest,:) = numberVals;
                        dest = dest + 1;
                    else
                        %fprintf('-');
                    end
                end
            end
            doubleVals = doubleVals(1:dest-1,:);
        end

        function [indexes] = findScreenFlips(~, analogVals, threshold, goHigh)
            %just work with the last colum of
            indexes = [];
            [len width] = size(analogVals);
            if width > 1
                analogVals = analogVals(:,end);
            end
            if goHigh
                for k = 2:len
                    if analogVals(k-1) < threshold && analogVals(k) >= threshold
                        indexes(end+1) = k;
                    end
                end
            else
                for k = 2:len
                    if analogVals(k-1) > threshold && analogVals(k) <= threshold
                        indexes(end+1) = k;
                    end
                end
            end

        end

        function [indexes] = findFlags(~, flags)
            indexes = [];
            len = length(flags);
            if(len > 1)
                for k = 2:len
                    if flags(k) ~= flags(k-1)
                        indexes(end+1) = k;
                    end
                end
            end
        end
        function [whichKey, pressTime, timedOut] = waitForKeys(obj, keys, untilTime)
            import superserial.Opcodes
            if superserial(Opcodes.BGStringReaderIsRunning) == 0
                disp('background thread must be running to waitForKeys')
                return
            end
            obj.resetDigitalState();
            pause(0.1); %make sure we really reset the digital state before we check for the first time
            if ~exist('keys', 'var')
                %wait for ANY key
                keys = 1:7;
            end
            if ~exist('untilTime', 'var')
                %default to 1 hr
                untilTime = inf;
            elseif untilTime <= 0
                untilTime = inf;
            end
            %disp('waiting for keys')
            %disp(keys)
            [~, digitalState, ~, ~] = obj.getLastNSamples(1);
            keys = min(keys, length(digitalState));
            keys = max(keys, 0);
            keyIndex = zeros(1,7);
            keyIndex(keys) = 1;
            timedOut = false;
            if any(digitalState(keys))
                disp('digital state holding old values')
            end
            while ~any(digitalState(keys)) && (untilTime > GetSecs)
                pause(0.01)
                [~, digitalState, ~, ~] = obj.getLastNSamples(1);
            end
            %get indexes, drop keys we shouldn't look at
            %below works because keys are just the index to the array
            whichKey = find(digitalState & keyIndex);
            pressTime = digitalState(whichKey);

            if isempty(whichKey)
                whichKey = 0;
                timedOut = true;
            end
            %output is array of keys that have been pressed, matching time stamps, bool if timedout
        end

        function [whichKey, pressTime] = kbcheck(obj, keys)
            %note: you probably want to call resetDigitalState() before using this
            import superserial.Opcodes
            if ~exist('keys', 'var')
                %wait for ANY key
                keys = 1:7;
            end
            if superserial(Opcodes.BGStringReaderIsRunning)
                [~, digitalState, ~, ~] = obj.getLastNSamples(1);
                keyIndex = zeros(1,7);
                keyIndex(keys) = 1;
                whichKey = find(digitalState & keyIndex);
                pressTime = digitalState(whichKey);
            else
                disp('kbcheck only works while the background thread is running')
                whichKey = [];
                pressTime = [];
            end
        end

        % function status = waitForValidStart(obj)
        %     import superserial.Opcodes
        %     while(superserial(Opcodes.BytesAvailable) < 1)
        %         obj.startAnalog();
        %         pause(0.01);
        %         %just waiting for the first data to start showing up
        %     end
        % end

        function [analog, digitalVals, flags, timeVals] = getLastNSamples(obj, N)
            import superserial.Opcodes
            lastStringData = superserial(Opcodes.BGStringReaderGetLastValue, N);
            lastData = obj.stringToDoubleMatrix(lastStringData);
            analog = lastData(:,1:6);
            digitalVals = lastData(:,7:13);
            flags = lastData(:, 14);
            timeVals = lastData(:,15);
        end

        function [analog, digitalVals, flags, timeVals] = getAllData(obj)
            import superserial.Opcodes
            lastStringData = superserial(Opcodes.BGStringReaderGetData);
            disp('copy complete, processing strings...')
            lastData = obj.stringToDoubleMatrix(lastStringData);
            analog = lastData(:,1:6);
            digitalVals = lastData(:,7:13);
            flags = lastData(:, 14);
            timeVals = lastData(:,15);
        end

        function resetDigitalState(~)
            %send Arduino reset command
            import superserial.Opcodes
            if 0 %superserial(Opcodes.BGStringReaderIsRunning)
                disp('Must stop background reader before sending any data');
            else
                outputBuffer = uint8('R');
                superserial(Opcodes.WriteBytes, outputBuffer);
            end
        end

        function obj = close(obj)
            import superserial.Opcodes
            if superserial(Opcodes.IsOpen)
                disp('# Closing serial port');
                superserial(Opcodes.BGStringReaderStop);
                superserial(Opcodes.Close);
            end
        end

        function newBaseline(obj, values)
            %should values be optional? If it isn't there, then use the last reading
            obj.baseline = obj.baseline + values(1:6);
        end

        function setTargetHz(obj, targetHz)
            %delay times are set to 0-10ms based on characters D:N
            import superserial.Opcodes
            % if superserial(Opcodes.BGStringReaderIsRunning)
            %     disp('Must stop background reader before sending any data');
            % else
                targetHz = min(targetHz, 10000);
                targetHz = max(targetHz, 1);
                superserial(Opcodes.WriteString, sprintf('D%d\n', targetHz));
            % end
            % superserial(Opcodes.ClearInputBuffer);
        end
        function startAnalog(obj)
            import superserial.Opcodes
            if superserial(Opcodes.BGStringReaderIsRunning)
                disp('Must stop background reader before sending any data');
            else
                superserial(Opcodes.WriteString, 'A');
                superserial(Opcodes.BGStringReaderStart);
            end
        end

        function sendFlag(~, newFlag)
            import superserial.Opcodes
            superserial(Opcodes.WriteString, sprintf('F%d\n', newFlag));
        end

        function clearBuffer(~)
            import superserial.Opcodes
            if superserial(Opcodes.BGStringReaderIsRunning)
                superserial(Opcodes.BGStringReaderStop);
                outputBuffer = uint8('Z');
                superserial(Opcodes.WriteBytes, outputBuffer);
                superserial(Opcodes.WriteString, 'C'); %special continue command
                superserial(Opcodes.BGStringReaderStart);
            else
                outputBuffer = uint8('Z');
                superserial(Opcodes.WriteBytes, outputBuffer);
                pause(1);
                superserial(Opcodes.BGStringReaderStart);
                superserial(Opcodes.BGStringReaderStop);
            end
        end

        function stopAnalog(obj)
            import superserial.Opcodes
            superserial(Opcodes.BGStringReaderStop);
            outputBuffer = uint8('Z');
            superserial(Opcodes.WriteBytes, outputBuffer);
            %superserial(Opcodes.ClearInputBuffer);
        end

        function [avail] = bytesAvailable(obj)
            import superserial.Opcodes
            avail = superserial(Opcodes.BytesAvailable);
        end

    end %end of methods section
end
