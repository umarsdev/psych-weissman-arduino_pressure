%use this with serialEcho on the Arduino to determine fastest round trip serial comms
portName = '/dev/cu.usbmodem1451';
s = serial(portName);
s.Baudrate = 115200;
fopen(s);
%%
tic
fwrite(s, 'A');
toc
returnedData = fread(s,1);
toc
%%
%open with psychtoolbox:
portName = '/dev/cu.usbmodem1451';
[handle, errmsg] = IOPort('OpenSerialPort', portName, ['BaudRate',115200])
%%
tic
IOPort('Write', handle, 'D');
time1 = toc
[readVal, ~, ~] = IOPort('Read', handle, 1, 1);
time2 = toc;
disp(time2-time1);
disp(readVal);
%%
IOPort('Close', handle);
fclose(s);